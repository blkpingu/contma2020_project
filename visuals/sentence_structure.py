import spacy
from spacy import displacy
from pathlib import Path

nlp = spacy.load("en_core_web_sm")
sent = "This is an example."

doc = nlp(sent)
svg = displacy.render(doc, style="dep", jupyter=False)
file_name = "pos" + ".svg"
output_path = Path("./" + file_name)
output_path.open("w", encoding="utf-8").write(svg)